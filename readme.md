Getting Started:

1. Clone the repo.
2. Run `go mod tidy` to install the dependencies for the server. 
3. Run `go run main.go` to start the server. The server listens on http://localhost:8888/
4. Go into the ui folder, run `npm install` to install the frontend dependencies.
5. Run `npm run dev` to start the UI. The UI application listens on http://localhost:5173/