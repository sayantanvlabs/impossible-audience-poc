module gitlab.com/sayantanvlabs/impossible-audience-poc

go 1.22.2

require (
	github.com/expr-lang/expr v1.16.9 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gdamore/tcell/v2 v2.7.1 // indirect
	github.com/go-chi/chi/v5 v5.0.13 // indirect
	github.com/go-chi/cors v1.2.1 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/tview v0.0.0-20240616192244-23476fa0bab2 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/term v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
