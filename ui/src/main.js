import './assets/main.css'

import { createApp } from 'vue'
import PrimeView from 'primevue/config'
import Aura from '@primevue/themes/aura'
import Tooltip from 'primevue/tooltip';

import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(PrimeView, {
    theme: {
        preset: Aura
    }
})

app.directive('tooltip', Tooltip);

app.use(router)

app.mount('#app')
