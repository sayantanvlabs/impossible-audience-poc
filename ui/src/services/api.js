import axios from "axios";

const apiClient = axios.create({
    baseURL: 'http://localhost:8888',
    headers: {
        'Content-Type': 'application/json'
    }
})

export default {
    getMnoAttributes() {
        return apiClient.get('/mno/attributes')
    },

    getAllAttributes() {
        return apiClient.get('/attributes')
    },

    validateAudience(data) {
        return apiClient.post("/audience/validate", data)
    }
}